import 'dart:convert';

import 'package:travel_app/main.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String message;
  UserDetails details;

  User({
    required this.message,
    required this.details,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        message: json["message"],
        details: UserDetails.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "loggedInUser": details.toJson(),
      };
}

class UserDetails {
  String id;
  String userName;
  String email;
  List<String> bookmarkedCities;
  String token;

  UserDetails({
    required this.id,
    required this.userName,
    required this.email,
    required this.bookmarkedCities,
    required this.token,
  });

  UserDetails copyWith({
    String? id,
    String? userName,
    String? email,
    List<String>? bookmarkedCities,
    String? token,
  }) =>
      UserDetails(
        id: id ?? this.id,
        userName: userName ?? this.userName,
        email: email ?? this.email,
        bookmarkedCities: bookmarkedCities ?? this.bookmarkedCities,
        token: token ?? this.token,
      );

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
        id: json["_id"],
        userName: json["userName"],
        email: json["email"],
        bookmarkedCities:
            List<String>.from(json["bookmarkedCities"].map((x) => x)),
        token: json["token"] ?? prefs!.getString('token'),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "userName": userName,
        "email": email,
        "bookmarkedCities": List<dynamic>.from(bookmarkedCities.map((x) => x)),
        "token": token,
      };
}
