import 'dart:convert';

Destinations destinationsFromJson(String str) =>
    Destinations.fromJson(json.decode(str));

String destinationsToJson(Destinations data) => json.encode(data.toJson());

class Destinations {
  String message;
  List<Destination> destinations;

  Destinations({
    required this.message,
    required this.destinations,
  });

  Destinations copyWith({
    String? message,
    List<Destination>? destinations,
  }) =>
      Destinations(
        message: message ?? this.message,
        destinations: destinations ?? this.destinations,
      );

  factory Destinations.fromJson(Map<String, dynamic> json) => Destinations(
        message: json["message"],
        destinations: List<Destination>.from(
            json["destinations"].map((x) => Destination.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "destinations": List<dynamic>.from(destinations.map((x) => x.toJson())),
      };
}

class Destination {
  String id;
  String cityName;
  String country;
  bool? bookmarkStatus;
  List<String> attractions;
  String cityInfo;
  List<String> cityImages;
  String rating;

  Destination({
    required this.id,
    required this.cityName,
    required this.country,
    this.bookmarkStatus,
    required this.attractions,
    required this.cityInfo,
    required this.cityImages,
    required this.rating,
  });

  Destination copyWith({
    String? id,
    String? cityName,
    String? country,
    bool? bookmarkStatus,
    List<String>? attractions,
    String? cityInfo,
    List<String>? cityImages,
    String? rating,
    bool? destinationBookmarkStatus,
  }) =>
      Destination(
        id: id ?? this.id,
        cityName: cityName ?? this.cityName,
        country: country ?? this.country,
        bookmarkStatus: bookmarkStatus ?? this.bookmarkStatus,
        attractions: attractions ?? this.attractions,
        cityInfo: cityInfo ?? this.cityInfo,
        cityImages: cityImages ?? this.cityImages,
        rating: rating ?? this.rating,
      );

  factory Destination.fromJson(Map<String, dynamic> json) => Destination(
        id: json["_id"],
        cityName: json["cityName"],
        country: json["country"],
        bookmarkStatus: json["bookmarkStatus"],
        attractions: List<String>.from(json["attractions"].map((x) => x)),
        cityInfo: json["cityInfo"],
        cityImages: List<String>.from(json["cityImages"].map((x) => x)),
        rating: json["rating"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "cityName": cityName,
        "country": country,
        "bookmarkStatus": bookmarkStatus,
        "attractions": List<dynamic>.from(attractions.map((x) => x)),
        "cityInfo": cityInfo,
        "cityImages": List<dynamic>.from(cityImages.map((x) => x)),
        "rating": rating,
      };
}

City cityFromJson(String str) => City.fromJson(json.decode(str));

String cityToJson(City data) => json.encode(data.toJson());

class City {
  Destination city;

  City({
    required this.city,
  });

  City copyWith({
    Destination? city,
  }) =>
      City(
        city: city ?? this.city,
      );

  factory City.fromJson(Map<String, dynamic> json) => City(
        city: Destination.fromJson(json["city"]),
      );

  Map<String, dynamic> toJson() => {
        "city": city.toJson(),
      };
}
