// // To parse this JSON data, do
// //
// //     final user = userFromJson(jsonString);

import 'dart:convert';

// User userFromJson(String str) => User.fromJson(json.decode(str));

// String userToJson(User data) => json.encode(data.toJson());

// class User {
//   String message;
//   RegisteredUser registeredUser;

//   User({
//     required this.message,
//     required this.registeredUser,
//   });

//   User copyWith({
//     String? message,
//     RegisteredUser? registeredUser,
//   }) =>
//       User(
//         message: message ?? this.message,
//         registeredUser: registeredUser ?? this.registeredUser,
//       );

//   factory User.fromJson(Map<String, dynamic> json) => User(
//         message: json["message"],
//         registeredUser: RegisteredUser.fromJson(json["registeredUser"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "message": message,
//         "registeredUser": registeredUser.toJson(),
//       };
// }

// class RegisteredUser {
//   String userName;
//   String email;
//   List<dynamic> bookmarkedCities;
//   String id;
//   String token;

//   RegisteredUser({
//     required this.userName,
//     required this.email,
//     required this.bookmarkedCities,
//     required this.id,
//     required this.token,
//   });

//   RegisteredUser copyWith({
//     String? userName,
//     String? email,
//     List<dynamic>? bookmarkedCities,
//     String? id,
//     String? token,
//   }) =>
//       RegisteredUser(
//         userName: userName ?? this.userName,
//         email: email ?? this.email,
//         bookmarkedCities: bookmarkedCities ?? this.bookmarkedCities,
//         id: id ?? this.id,
//         token: token ?? this.token,
//       );

//   factory RegisteredUser.fromJson(Map<String, dynamic> json) => RegisteredUser(
//         userName: json["userName"],
//         email: json["email"],
//         bookmarkedCities:
//             List<dynamic>.from(json["bookmarkedCities"].map((x) => x)),
//         id: json["_id"],
//         token: json["token"],
//       );

//   Map<String, dynamic> toJson() => {
//         "userName": userName,
//         "email": email,
//         "bookmarkedCities": List<dynamic>.from(bookmarkedCities.map((x) => x)),
//         "_id": id,
//         "token": token,
//       };
// }

String userToJson(User data) => json.encode(data.toJson());

class User {
  String message;
  String token;
  String userId;
  String userEmail;
  List<dynamic> userBookmarkedCities;

  User({
    required this.message,
    required this.token,
    required this.userId,
    required this.userEmail,
    required this.userBookmarkedCities,
  });

  User copyWith({
    String? message,
    String? token,
    String? userId,
    String? userEmail,
    List<dynamic>? userBookmarkedCities,
  }) =>
      User(
        message: message ?? this.message,
        token: token ?? this.token,
        userId: userId ?? this.userId,
        userEmail: userEmail ?? this.userEmail,
        userBookmarkedCities: userBookmarkedCities ?? this.userBookmarkedCities,
      );

  factory User.fromJson(Map<String, dynamic> json) => User(
        message: json["message"],
        token: json["token"],
        userId: json["userId"],
        userEmail: json["userEmail"],
        userBookmarkedCities:
            List<dynamic>.from(json["userBookmarkedCities"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "token": token,
        "userId": userId,
        "userEmail": userEmail,
        "userBookmarkedCities":
            List<dynamic>.from(userBookmarkedCities.map((x) => x)),
      };
}
