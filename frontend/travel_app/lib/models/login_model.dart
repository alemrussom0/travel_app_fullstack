import 'dart:convert';

String userToJson(User data) => json.encode(data.toJson());

class User {
  String message;
  String token;
  String userId;
  String userEmail;
  List<dynamic> userBookmarkedCities;

  User({
    required this.message,
    required this.token,
    required this.userId,
    required this.userEmail,
    required this.userBookmarkedCities,
  });

  User copyWith({
    String? message,
    String? token,
    String? userId,
    String? userEmail,
    List<dynamic>? userBookmarkedCities,
  }) =>
      User(
        message: message ?? this.message,
        token: token ?? this.token,
        userId: userId ?? this.userId,
        userEmail: userEmail ?? this.userEmail,
        userBookmarkedCities: userBookmarkedCities ?? this.userBookmarkedCities,
      );

  factory User.fromJson(Map<String, dynamic> json) => User(
        message: json["message"],
        token: json["token"],
        userId: json["userId"],
        userEmail: json["userEmail"],
        userBookmarkedCities:
            List<dynamic>.from(json["userBookmarkedCities"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "token": token,
        "userId": userId,
        "userEmail": userEmail,
        "userBookmarkedCities":
            List<dynamic>.from(userBookmarkedCities.map((x) => x)),
      };
}
