class EmailAlreadyExistsException implements Exception {}

class BadRequestException implements Exception {}

class NoInternetConnectionException implements Exception {}
