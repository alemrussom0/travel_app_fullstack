import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart';
import 'package:travel_app/main.dart';
import 'package:travel_app/models/destinations_model.dart';
import 'package:travel_app/models/user_model.dart';

class DestinationServices {
  String api = "localhost:3000/";
  // static String endpoint = 'http://192.168.100.8:3000';
  // String endpoint = 'http://localhost:3000';
  static String endpoint = "https://travel-app-3wib.onrender.com";
  static Client? client = Client();
  static Map<String, String> requestHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };

  static String? serverMessage;

  static Future login({required String email, required String password}) async {
    log('email: $email, password: $password');

    var body = {
      "email": email,
      "password": password,
    };

    var response = await client!.post(
      Uri.parse("$endpoint/login"),
      body: jsonEncode(body),
      headers: requestHeaders,
    );
    try {
      if (response.statusCode == 200) {
        prefs!.setString("token", response.body);

        return userFromJson(response.body).details;
      } else {
        log('Response: ${response.statusCode}, ${response.body}');
        return "Opps Something went wrong: ${response.statusCode}";
      }
    } catch (err) {
      log('Error: $err');
      rethrow;
    }
  }

  static Future signUp(
      {required String fullName,
      required String email,
      required String password}) async {
    var body = {
      "userName": fullName,
      "email": email,
      "password": password,
      "bookmarkedCities": []
    };

    var response = await client!.post(
      Uri.parse("$endpoint/signUp"),
      body: jsonEncode(body),
      headers: requestHeaders,
    );

    var message = jsonDecode(response.body);

    log('New Message: ${message["message"]}');

    try {
      if (response.statusCode == 201) {
        log('Response: ${response.body}');
        serverMessage = "User successfully created";
        prefs!.setString("token", response.body);
        return userFromJson(response.body).details;
      } else {
        serverMessage = "Email already exits";
        return serverMessage;
      }
    } catch (err) {
      serverMessage = err.toString();
      rethrow;
    }
  }

  static Future<User> bookmarkOrUnbookmarkDestination(
      String destinationId) async {
    var userId = prefs!.getString('userId');
    log('userId: $userId');
    log("destinationId: $destinationId");

    // Make a PUT request to the backend to update the user's bookmarks
    final response = await client!.put(
      Uri.parse('$endpoint/users/$userId/bookmarks'),
      body: jsonEncode({
        "userId": userId,
        "destinationId": destinationId,
      }),
      headers: requestHeaders,
    );

    if (response.statusCode == 200) {
      return userFromJson(response.body);
    } else {
      log("Response: ${response.statusCode}");
      return userFromJson(response.body);
    }
  }

  static Future<List<Destination>> fetchDestinations() async {
    try {
      var response = await client!.get(Uri.parse("$endpoint/getDestinations"));
      return destinationsFromJson(response.body).destinations;
    } on Exception catch (err) {
      log('Error: $err');
      rethrow;
    }
  }

  static Future searchDestination({String? cityName}) async {
    log('City: $cityName');
    try {
      var response =
          await client!.get(Uri.parse("$endpoint/searchCity/$cityName"));

      if (response.statusCode == 404) {
        return 'City Not found';
      }

      return cityFromJson(response.body).city;
    } catch (err) {
      rethrow;
    }
  }

  static Future<bool> logout() async {
    await client!.get(Uri.parse("$endpoint/logout"));
    prefs!.remove('token');
    return true;
  }
}
