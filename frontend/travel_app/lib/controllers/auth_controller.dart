import 'dart:developer';

import 'package:email_validator/email_validator.dart';
import 'package:travel_app/models/user_model.dart';

import '../main.dart';
import '../services/services.dart';

class AuthController {
  String fullName = '';
  String email = '';
  String password = '';

  String? validateEmail(String? email) {
    if (email == null || email.isEmpty) {
      return 'Please enter an email address.';
    } else if (!EmailValidator.validate(email)) {
      return 'Please enter a valid email address.';
    } else {
      return null;
    }
  }

  String? validatePassword(String? password) {
    if (password == null || password.isEmpty) {
      return 'Please enter a password';
    } else if (password.length < 6) {
      return 'Password should be at least 8 characters';
    } else {
      return null;
    }
  }

  String? validateName(String? name) {
    if (name == null) {
      return "Please enter a name";
    } else {
      return null;
    }
  }

  Future<dynamic> userlogin(String email, String password) async {
    UserDetails response = await DestinationServices.login(
      email: email,
      password: password,
    );

    log('Response: ${response}');
    prefs!.setString("userId", response.id);
    prefs!.setString("token", response.token);
    prefs!.setString("email", "response.userEmail");
    prefs!.setStringList("bookmarks", response.bookmarkedCities);
    return response;
  }

  Future register(
      {required String fullName,
      required String email,
      required String password}) async {
    return await DestinationServices.signUp(
      fullName: fullName,
      email: email,
      password: password,
    );
  }
}
