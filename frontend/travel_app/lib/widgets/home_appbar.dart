import 'package:flutter/material.dart';
import 'package:travel_app/screens/welcome.dart';
import 'package:travel_app/services/services.dart';
import 'package:travel_app/widgets/auth_form.dart';
import 'package:travel_app/widgets/post_screen.dart';

class HomeAppBar extends StatefulWidget {
  HomeAppBar({super.key});

  @override
  State<HomeAppBar> createState() => _HomeAppBarState();
}

class _HomeAppBarState extends State<HomeAppBar> {
  TextEditingController searchController = TextEditingController();

  String search = '';
  bool isSearch = false;

  Widget showSearchBar(BuildContext context) {
    return SizedBox(
      width: 250,
      child: TextFormField(
        controller: searchController,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.w500,
        ),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: const Icon(
              Icons.search,
              color: Colors.black,
              size: 30,
            ),
            onPressed: () async {
              await searchCity(context);
            },
          ),
          label: const Text(
            "search",
            style: TextStyle(
              color: Colors.black,
              fontSize: 16,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Colors.grey, width: 1.5),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Colors.grey, width: 1.5),
          ),
        ),
        onChanged: (value) {
          setState(() {
            search = searchController.text;
          });
        },
      ),
    );
  }

  Future<void> searchCity(BuildContext context) async {
    var navigator = Navigator.of(context);
    var scaffoldMessenger = ScaffoldMessenger.of(context);
    var destination = await DestinationServices.searchDestination(
        cityName: searchController.text);

    if (destination == 'City Not found') {
      scaffoldMessenger.showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: Text(
          "Opps! $destination. Please try again with the correct city.",
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
        ),
      ));
    } else {
      navigator.push(
        MaterialPageRoute(
          builder: (context) => PostScreen(destinations: destination),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            showSearchBar(context),
            InkWell(
              onTap: () async {
                var navigator = Navigator.of(context);
                var isLoggedOut = await DestinationServices.logout();

                if (isLoggedOut) {
                  navigator.pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => const WelcomeScreen(
                        child: AuthForm(),
                      ),
                    ),
                  );
                }
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 6,
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Icon(
                  // Icons.sort_rounded,
                  Icons.logout_outlined,
                  size: 28,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
