const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

const jwtSecret = process.env.JWT_SECRET;

exports.authenticateToken = async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  console.log("token ", req.token);

  if (!token) {
    return res.status(401).json({ error: "No token provided" });
  }

  jwt.verify(token, jwtSecret, (err, user) => {
    if (err) {
      return res.status(403).json({ error: "Invalid Token" });
    }

    req.user = user;
    next();
  });
};

exports.generateToken = (user) => {
  const payload = {
    id: user.id,
    userName: user.userName,
  };

  const token = jwt.sign(payload, jwtSecret);

  return token;
};

exports.isLoggedIn = async (req, res, next) => {
  // const token = req.headers["authorization"];
  const token = req.headers.authorization;
  console.log("authHeader: ", req.headers.authorization);

  console.log("Token: ", token);

  try {
    const decodedToken = jwt.verify(token, jwtSecret);

    req.user = decodedToken;
  } catch (error) {
    res.status(401).json({ error: "Unauthorized Use" });
    return;
  }

  next();
};
