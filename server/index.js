const express = require("express");

const dotenv = require("dotenv");
const bodyParser = require("body-parser");

const desRouters = require("./controller/desitnation_controller");
const userRoutes = require("./controller/userControllers");
const authenticate = require("./middleWare/auth");

const app = express();

app.use(bodyParser.json());
dotenv.config();

const port = 3000;
const PORT = process.env.PORT;

app.route("/login").post(userRoutes.login);
app.route("/register").post(userRoutes.register);

// app.use(authenticate.isLoggedIn);

// user routes

app.route(`/users`, authenticate.authenticate).get(userRoutes.get_users);
app.route("/userById/:id").get(userRoutes.user_data);
app.route("/users/:userId/bookmarks").put(userRoutes.updateBookmarks);
app.route("/logout").get(userRoutes.logout);

// Destination routers
app.route("/getDestinations").get(desRouters.getDestinations);
app.route("/addDestination").post(desRouters.addDestination);
app.route("/destinations/:id").get(desRouters.getDestinationById);
app.route("/searchCity/:cityName").get(desRouters.searchDestination);
app.route("/updateDestination/:id").put(desRouters.updateDestination);
app.route("/deleteDestination/:id").delete(desRouters.deleteDestination);

// Error handling middleware
app.use(function (err, req, res, next) {
  // Log the error
  console.log(err);

  // Send a 500 Internal Server Error response
  res.status(500).json({ message: "Internal Server Error" });
});

app.listen(port, () => console.log(`listening on http://localhost:${port}`));
