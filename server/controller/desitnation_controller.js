const { ObjectId } = require("mongodb");
const dotenv = require("dotenv");
const MongoClient = require("mongodb").MongoClient;

dotenv.config();

const mongo_url = process.env.MONGODB_URL;
const client = new MongoClient(mongo_url);
const db = client.db("travelApp");
const destinationsCollection = db.collection("destinations");

exports.getDestinations = async (req, res) => {
  const destinations = await destinationsCollection.find().toArray();
  try {
    res.status(200).json({ message: "Success", destinations });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.addDestination = async (req, res) => {
  // Get the city data from the request body
  const city = {
    cityName: req.body.cityName,
    country: req.body.country,
    bookmarkStatus: req.body.bookmarkStatus,
    cityInfo: req.body.cityInfo,
    attractions: req.body.attractions,
    cityImages: req.body.cityImages,
  };

  console.log("CityName: ", req.body.cityName);

  // Send a success response to the client
  try {
    // Insert the city into the database
    await destinationsCollection.insertOne(city);
    res.status(201).json({ message: "City created successfully", city });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.getDestinationById = async (req, res) => {
  const destinationId = ObjectId.createFromHexString(req.params.id);
  const city = await destinationsCollection.findOne({ _id: destinationId });

  try {
    if (!city) {
      return res.status(404).json({ message: "City not found" });
    }
    res.status(200).json({ message: "City Successfully fetched.", city });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.searchDestination = async (req, res) => {
  const cityName = req.params.cityName;

  // Create a regular expression that matches the city name regardless of the spelling
  const cityNameRegex = new RegExp(cityName, "i");

  const city = await destinationsCollection.findOne({
    cityName: cityNameRegex,
  });

  try {
    if (!city) {
      return res.status(404).json({ message: "City not found" });
    }
    res.status(200).json({ city });
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.updateDestination = async (req, res) => {
  const destinationId = ObjectId.createFromHexString(req.params.id);
  console.log("destinationId ", destinationId);

  const updateDest = req.body;
  console.log(`Body:  ${req.body.cityName}`);

  const city = await destinationsCollection.findOne({ _id: destinationId });

  try {
    if (city) {
      await destinationsCollection.updateOne(
        { _id: destinationId },
        { $set: updateDest }
      );
      res.status(200).json({ message: "Location update successfully", city });
    } else {
      console.log("city ", city);
      return res
        .status(404)
        .json({ message: "Failed to update destination. No City found" });
    }
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.deleteDestination = async (req, res) => {
  const destinationId = ObjectId.createFromHexString(req.params.id);
  console.log(`destinationId: ${destinationId}`);

  const city = await destinationsCollection.findOne({ _id: destinationId });
  console.log("city: ", city);

  try {
    if (city) {
      await destinationsCollection.deleteOne({ _id: destinationId });
      res.status(200).json({ message: "Location Deleted successfully", city });
    } else {
      console.log(`City: ${city}`);
      return res
        .status(404)
        .json({ message: "Failed to update destination. No City found" });
    }
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};
