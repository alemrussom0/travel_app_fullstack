const bcrypt = require("bcryptjs");
const { ObjectId } = require("mongodb");
const dotenv = require("dotenv");
const MongoClient = require("mongodb").MongoClient;
const jwt = require("jsonwebtoken");
const Joi = require("joi");

const authenticate = require("../middleWare/auth");

dotenv.config();

const mongo_url = process.env.MONGODB_URL;
const client = new MongoClient(mongo_url);
const db = client.db("travelApp");
const usersCollection = db.collection("users");

const registerSchema = Joi.object({
  userName: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required(),
  bookmarkedCities: Joi.array().items(Joi.string()),
});

async function checkEmailStatus(email) {
  const user = await usersCollection.findOne({ email });
  return user !== null;
}

async function fetchUserData(id) {
  const objectId = ObjectId.createFromHexString(id);
  const user = usersCollection.findOne({ _id: objectId });
  return user;
}

exports.register = async (req, res) => {
  const validationResult = registerSchema.validate(req.body);
  if (validationResult.error) {
    res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }

  const newUser = {
    userName: req.body.userName,
    email: req.body.email,
    password: req.body.password,
    bookmarkedCities: [],
  };

  if (!newUser.userName || !newUser.email || !newUser.password) {
    return res.status(400).json({ message: "All fields are required" });
  }

  const emailExists = await checkEmailStatus(newUser.email);

  if (emailExists) {
    return res.status(409).json({ message: "Email already exists" });
  }

  try {
    await usersCollection.insertOne(newUser);
    const token = jwt.sign({ id: newUser._id }, process.env.JWT_SECRET);
    const user = Object.assign(newUser);
    delete user.password;
    user.token = token;
    return res.status(201).json({
      message: "User created successfully",
      user: user,
    });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.login = async (req, res) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password) {
      return res.status(400).json({ message: "All fields are required" });
    }

    // Find the user in the database
    const loggedUser = await usersCollection.findOne({ email });

    // If the user does not exist, return an error response
    if (!loggedUser) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    // Validate the password
    var isPasswordValid = await bcrypt.compare(password, loggedUser.password);
    console.log("isPasswordValid", isPasswordValid);

    if (isPasswordValid) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const user = Object.assign(loggedUser);
    delete user.password;

    user.token = authenticate.generateToken(user);

    res.setHeader("Authorization", `Bearer ${user.token}`);
    console.log(`HEADERS: ${req.headers.authorization}`);

    res.status(200).json({
      message: "User successfully logged in",
      user: user,
    });
  } catch (error) {
    console.log(`Error: ${error}`);
    res.json({ message: error.message });
  }
};

exports.get_users = async (req, res) => {
  try {
    const users = await usersCollection.find().toArray();

    res.status(200).json({ message: "Users successfully fetched", users });
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.user_data = async (req, res) => {
  const id = req.params.id;

  try {
    const user = await fetchUserData(id);
    if (!user) {
      return res.status(400).json({ message: "User not found" });
    }
    return res.status(200).json({ user });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};

exports.updateBookmarks = async (req, res) => {
  const userId = req.params.userId;
  const destinationId = req.body.destinationId;

  // Find the user in the database
  const user = await usersCollection.findOne({ _id: new ObjectId(userId) });

  // Initialize bookmarkedCities as an empty array if it's null
  user.bookmarkedCities ?? [];

  // update the user's bookmarkedCities array
  if (user.bookmarkedCities.includes(destinationId)) {
    const index = user.bookmarkedCities.indexOf(destinationId);
    user.bookmarkedCities.splice(index, 1);
  } else {
    user.bookmarkedCities.push(destinationId);
  }

  await usersCollection.updateOne(
    { _id: new ObjectId(userId) },
    { $set: user }
  );
  res.status(200).json({ message: "Bookmarks updated successfully", user });
};

exports.logout = async (req, res) => {
  // Invalidate the user's JWT
  res.clearCookie("Authorization");
  try {
    // Send the response to the user indicating the user has been logged out
    res.status(200).json({ message: "User logged out successfully" });
  } catch (error) {
    console.log(`Error in catch: ${error}`);
    return res.status(500).json({ message: error.message });
  }
};
